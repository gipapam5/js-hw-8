1. Метод forEach дозволяє нам виконати якусь операцію для кожного параметру масиву. Наприклад
   const arr = [1, 2, 3, 4, 5];
   arr.forEach((num) => {
   console.log(num += 'chips');
   });
   У цьому випакдку до кожного елементу масиву додасться слово chips.
2. Мутують, тобто змінюють вихідний масив такі методи як: splice, push, unshift, shift, pop. Наприклад
   const arr = [1, 2, 3, 4, 5];
   arr.push('Hello!');
   console.log(arr);
   Немутучі, тобто методи які не змінюють вихідний масив це: slice, map, reduce, filter, sort,concat. Наприклад
   const arr = [1, 2, 3, 4, 5];
   const newArr = [6, 7, 8, 9];
   const addArr = arr.concat(newArr);
   console.log(arr);
   console.log(newArr);
   console.log(addArr)
3. Для перевірки чи в змінну ініціалізовано масив можна використати глобальну функцію Array з методом isArray(). Наприклад
   const arr = [1, 2, 3, 4, 5];
   console.log(Array.isArray(arr)); // true
   const newArr = {};
   console.log(Array.isArray(newArr)); // false
4. Метод map краще використовувати - якщо ми хочемо перебрати vfcbd і якимось чином його змінити, вплинути на нього і результатом цього отримати новий масив. А метод forEach краще використовувати просто для перебору масиву для зміни вхідного масиву, не отримуючи нового.
