// const arr = ["travel", "hello", "eat", "ski", "lift"];
// const count = arr.filter((elem) => { return elem.length > 3 }).length;
// console.log(count);

// const users = [
//     {
//         name: 'Ivan',
//         age: 25,
//         sex: "чоловіча"
//     },
//     {
//         name: 'Ivanka',
//         age: 20,
//         sex: "жіноча"
//     },
//     {
//         name: 'Petrivna',
//         age: 65,
//         sex: "жіноча"
//     },
//     {
//         name: 'Petro',
//         age: 34,
//         sex: "чоловіча"
//     },
// ];

// let filteredArr = users.filter((elem) => {
//     if (elem.sex === 'чоловіча') {
//         return elem;
//     }
// });
// console.log(filteredArr);

function filterBy(arr, dataType) {
    let filteredArr = arr.filter((elem) => {
        if (typeof elem !== dataType || elem === null) {
            return true;
        };
    });
    return filteredArr;
}
console.log(filterBy([1, 2, 3, '21', null, {}], 'number'));